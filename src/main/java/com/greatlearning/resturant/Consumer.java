package com.greatlearning.resturant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class Consumer {

    @Autowired
    private MessageRepository messageRepo;

    @KafkaListener(topics = "${myapp.kafka.admin-topic}", groupId = "group_id")
    public void consumeMessageForAdmin(String message) {
        String decryptedMessage = AesEncryptDecryptConfig.decrypt(message);
        System.out.println("Consumer: Message received by Admin from User and decrypted: " + decryptedMessage);
        messageRepo.addAdminMessage(decryptedMessage);
    }

    @KafkaListener(topics = "${myapp.kafka.user-topic}", groupId = "group_id")
    public void consumeMessageForUserFromAdmin(String message) {
        String decryptedMessage = AesEncryptDecryptConfig.decrypt(message);
        System.out.println("Consumer: Message received by User from Admin and decrypted: " + decryptedMessage);
        messageRepo.addUserMessage(decryptedMessage);
    }

    @KafkaListener(topics = "${myapp.kafka.user-to-user-topic}", groupId = "group_id")
    public void consumeMessageFromUserToUser(String message) {
        String decryptedMessage = AesEncryptDecryptConfig.decrypt(message);
        System.out.println("Consumer: Message received by User from another User and decrypted: " + decryptedMessage);
        messageRepo.addUserMessageFromOtherUser(decryptedMessage);
    }

}
