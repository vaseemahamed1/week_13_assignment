package com.greatlearning.resturant;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/kafka")
public class ChatController {

    @Autowired
    private Producer producer;

    @Autowired
    private MessageRepository messageRepo;

    @PostMapping("/sendToAdmin")
    @ApiOperation(value = "Send messages to admin from user")
    public void messageToAdmin(@RequestParam("message") String message) {
        this.producer.sendMessageToAdmin(message);
    }

    @PostMapping("/sendToUserFromAdmin")
    @ApiOperation(value = "Send messages to user from admin")
    public void messageToUserFromAdmin(@RequestParam("message") String message) {
        this.producer.sendMessageToUserFromAdmin(message);
    }

    @PostMapping("/sendToUserFromUser")
    @ApiOperation(value = "Send messages to user form other users")
    public void messageToUserFromUser(@RequestParam("message") String message) {
        this.producer.sendMessageToUserFromUser(message);
    }

    //Read all messages
    @GetMapping("/getAllAdminMessages")
    @ApiOperation(value = "Get all messages sent to admin from user")
    public String getAllAdminMessages() {
        return messageRepo.getAllAdminMessages();
    }

    //Read all messages
    @GetMapping("/getAllUserMessagesFromAdmin")
    @ApiOperation(value = "Get all messages sent to user from admin")
    public String getAllUserMessagesFromAdmin() {
        return messageRepo.getAllUserMessages();
    }

    //Read all messages
    @GetMapping("/getAllUserMessagesFromOtherUsers")
    @ApiOperation(value = "Get all messages sent to user from other users")
    public String getAllUserMessagesFromOtherUsers() {
        return messageRepo.getAllUserMessagesFromOtherUser();
    }
}
