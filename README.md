#### For enabling https follow below steps

- navigate to jdk_1.8/bin folder
- open the cmd prompt and run the command
    - keytool -genkeypair -alias springbootkafka -keyalg RSA -keysize 4096 -storetype PKCS12 -keystore springbootkafka.p12 -validity 3650 -storepass password
- fill the required details to generate the key
- copy the generated key to resource folder
- update the configuration in application.yml
- now http is enabled, use https://localhost:8087/apis

#### The message chat between admin and user is encrypted and decrypted using AES algorithm
- private key used: "aesGreatLearning"


//Download Kalfa latest version, use below Link:
https://mirrors.estointernet.in/apache/kafka/2.8.0/kafka_2.13-2.8.0.tgz

store the kafka in c drive and rename the kafka folder to kafka

// Some config changes, change log folder:
Open kafka/config/server.properties
search logs and change the path to custom Path
log.dirs=<kafka directory>/kafka-logs

Open kafka/config/zookeeper.properties
change dataDir= <kafka directory>/zookeeper.properties

You can start the Zookeeper and Kafka servers by using the below commands.
Depending on your OS and your file path, run the below commands in new CMD's
For Windows operating system

Start Zookeeper :
$ .\bin\windows\zookeeper-server-start.bat .\config\zookeeper.properties

Start Kafka server:
$ .\bin\windows\kafka-server-start.bat .\config\server.properties

Start up the spring boot kafka application
Topic will be automatically created based on the project configuration

swagger can be used to do the chat operation between users and admin respectively
